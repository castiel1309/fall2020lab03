//Castiel Le ID:1933080
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTest {
	
	@Test
	void testGetter() {
		Vector3d testvector0 = new Vector3d(7, 8, 9);
		assertEquals(7, testvector0.getX());
		assertEquals(8, testvector0.getY());
		assertEquals(9, testvector0.getZ());
	}

	@Test
	
	void testMagnitude() {
		Vector3d testvec1 = new Vector3d(1, 2, 3);
		assertEquals(3.7416573867739413, testvec1.magnitude());
	}
	
	@Test
	
	void testDotProduct() {
		Vector3d testvec2 = new Vector3d(1, 2, 3);
		Vector3d testvec3 = new Vector3d(2, 3 ,4);
		assertEquals(20.0, testvec2.dotProduct(testvec3));
	}
	
	@Test
	
	void testAdd() {
		Vector3d testvec4 = new Vector3d(2, 3, 4);
		Vector3d testvec5 = new Vector3d(5, 6, 7);
		Vector3d testvec6 = testvec4.add(testvec5);
		assertEquals(7, testvec6.getX());
		assertEquals(9, testvec6.getY());
		assertEquals(11, testvec6.getZ());
	}

}
