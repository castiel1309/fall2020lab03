//Castiel Le ID:1933080
package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}

	public double magnitude() {
		return Math.sqrt(x * x + y * y + z * z);
	}
	
	public double dotProduct(Vector3d secondvec) {
		return (x * secondvec.getX() + y * secondvec.getY() + z * secondvec.getZ());
	}
	
	public Vector3d add(Vector3d secondvec) {
		double newX = x + secondvec.getX();
		double newY = y + secondvec.getY();
		double newZ = z + secondvec.getZ();
		Vector3d newvec = new Vector3d(newX, newY, newZ);
		return newvec;
	}
}
